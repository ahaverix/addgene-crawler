select * from detailedView
where detailed_id not in (
    select max(detailed_id)
    from detailedView x
    where detailedView.plasmid_code = x.plasmid_code
    and detailedView.year = x.year
	and detailedView.authors = x.authors
	and detailedView.key = x.key
) ;

select * from crawler
where crawl_id not in (
    select max(crawl_id)
    from crawler x
    where crawler.plasmid_code = x.plasmid_code
    and crawler.year = x.year
) ;
