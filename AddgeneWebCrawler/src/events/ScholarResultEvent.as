package events
{
	import flash.events.Event;
	
	import mx.rpc.events.FaultEvent;
	
	public class ScholarResultEvent extends Event
	{
		public static var COMPLETE:String = "scholar_result_complete";
		public static var SKIP:String = "scholar_result_skip";
		public static var ERROR:String = "scholar_result_error";
		public static var CAPTCHA_DETECTED:String = "scholar_result_captcha_detected";
		
		public var fault:FaultEvent;
		
		public function ScholarResultEvent(type:String, fault:FaultEvent=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}