package events
{
	import flash.events.Event;
	
	public class ProcessDialogEvent extends Event
	{
		public static var START:String = "process_dialog_start";
		
		private var plasmicCodes:Array;
		
		public function ProcessDialogEvent(type:String, codes:Array, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this.plasmicCodes = codes;
		}
		
		public function get PlasmidCodes():Array
		{
			return this.plasmicCodes;
		}
	}
}