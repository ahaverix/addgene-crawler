package components
{
	import mx.collections.ArrayCollection;
	import mx.controls.ComboBox;
	import mx.formatters.DateFormatter;
	
	public class DateComboBox extends ComboBox
	{
		public function DateComboBox()
		{
			super();
			
			var formatter:DateFormatter = new DateFormatter();
			formatter.formatString = "YYY";
			dataProvider = new ArrayCollection();
			
			var yearsToAdd:Number = new Date().fullYear - 2004;
			for (var i:int=0; i<yearsToAdd + 1; i++) {
				var currentYear:Number = 2004 + i;
				var date:Date = new Date(currentYear,1,1);
				
				ArrayCollection(dataProvider).addItem(formatter.format(date));
			}
			//selectedIndex = 0;
		}
	}
}