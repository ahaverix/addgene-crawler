import flash.events.Event;
import flash.events.SQLErrorEvent;
import flash.utils.Dictionary;
import flash.utils.setTimeout;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.StringUtil;

import events.ProcessDialogEvent;

import managers.configuration.ConfigurationsManager;
import managers.log.LoggerManager;
import managers.parsers.PlasmidSearchManager;
import managers.parsers.events.SearchManagerEvent;
import managers.sql.Query;
import managers.sql.SQLManager;
import managers.sql.events.SQLManagerEvent;

import models.ScholarResult;

import views.dialogs.ConfigurationDialog;
import views.dialogs.LoadingDialog;
import views.dialogs.ProcessDialog;
import views.dialogs.ScholarBrowser;


[Bindable]
public var sqlManager:SQLManager;
private var searchManager:PlasmidSearchManager;
private var confsManager:ConfigurationsManager = ConfigurationsManager.sharedManager;

[Bindable]
private var isProcessing:Boolean = false;
private var loadingDialog:LoadingDialog

// Filter variables
private var filterKeyword:String = "";

// On creation complete
protected function onInit(event:FlexEvent):void 
{
	LoggerManager.logInfo("Application started");
	
	// Initialize managers
	sqlManager = new SQLManager();
}

private function onResize():void
{
	if(this.width > 750)
		mainNavigator.setStyle("tabWidth", (this.width - 30) / 3);
}

/**
 * Search for all the available plasmid codes
 */
private function processPlasmids():void
{	
	var processDlg:ProcessDialog = PopUpManager.createPopUp(this, ProcessDialog, true) as ProcessDialog;
	processDlg.allPlasmidsCollection = new ArrayCollection(sqlManager.allPlasmids.source);
	processDlg.addEventListener(ProcessDialogEvent.START, startProcessingCodes);
	PopUpManager.centerPopUp(processDlg);
}

private function startProcessingCodes(event:ProcessDialogEvent):void
{
	LoggerManager.logInfo("Attempting to process codes...");
	LoggerManager.logInfo(event.PlasmidCodes.join("|"));
	
	var plasmidCodes:Array = event.PlasmidCodes;
	
	loadingDialog = PopUpManager.createPopUp(this, LoadingDialog, true) as LoadingDialog;
	loadingDialog.total = plasmidCodes.length;
	loadingDialog.addEventListener(FlexEvent.HIDE, stopSearching);
	PopUpManager.centerPopUp(loadingDialog);
	
	isProcessing = true;

	// Initialize search manager
	searchManager = new PlasmidSearchManager(sqlManager);
	searchManager.dispatcher.addEventListener(SearchManagerEvent.PROGRESS, onSearchProgress);
	searchManager.dispatcher.addEventListener(SearchManagerEvent.CAPTCHA_DETECTED, onCaptchaDetected);
	
	// Start the search
	searchManager.searchPlasmidsList(plasmidCodes, stopSearching, onGenericError);
}

private function onCaptchaDetected(event:Event):void
{
	// In case search stopped in the middle, we want to save what we got so far
	if(searchManager)
	{
		LoggerManager.logError("Error occured, attempting to recover any crawled results...");
		savePlasmidResults(searchManager.plasmidCode, searchManager.listUniqueResults[searchManager.plasmidCode], false)
	}
	
	stopSearching();
	
	// Show browser
	showBrowser();
}

private function onSearchProgress(event:SearchManagerEvent):void
{
	LoggerManager.logInfo("Search progress...");
	
	if(loadingDialog)
		loadingDialog.setCurrent(searchManager.currentPlasmidIndex + 1); 
	
	if(event.PlasmidCode.length > 0 && event.SearchResults)
	{
		LoggerManager.logInfo("Attempting to save " + event.PlasmidCode);
		savePlasmidResults(event.PlasmidCode, event.SearchResults, !event.SearchHasErrors);
	}
}

private function savePlasmidResults(plasmidCode:String, searchResults:Dictionary, searchCompleted:Boolean=true):void
{
	if(plasmidCode.length > 0 && searchResults)
	{
		LoggerManager.logInfo("Saving " + plasmidCode);
		
		// Update plasmid status
		var searchStatus:String = searchCompleted ? "1" : "2";
		sqlManager.executeQuery(Query.UPDATE_PLASMID_STATUS.replace("$code", plasmidCode).replace("$status", searchStatus));
		
		// Update plasmid total results
		var totalCount:Number = isNaN(searchManager.currentPossibilityTotalCount) ? 0 : searchManager.currentPossibilityTotalCount;
		sqlManager.executeQuery(Query.UPDATE_PLASMID_TOTAL.replace("$code", plasmidCode).replace("$total", totalCount.toString()));
		
		groupAndStorePlasmidResults(plasmidCode, searchResults);
	}
}

/**
 * Set status as not searching
 * Hides loading dialog
 * Reload all results
 */
private function stopSearching(event:Event=null):void
{
	LoggerManager.logInfo("Attempting to stop searching...");
	
	if(searchManager)
		searchManager.abort();
	
	isProcessing = false;
	
	// Reload grid from database
	sqlManager.loadAll();
	
	// Stop processing
	if(loadingDialog)
		loadingDialog.close();
}


/**
 * Parse every plasmid scholar results
 * Group Scholar results by year
 * Store results in the database
 * 
 * @param plasmidCode= Plasmid code (key)
 * @param uniqueResults= Dictionary contains list of ScholarResult
 */
private function groupAndStorePlasmidResults(plasmidCode:String, uniqueResults:Dictionary):void
{
	LoggerManager.logInfo("Group and store plasmid results...");
	// Lets store and refresh results
	var reducedResults:Dictionary = new Dictionary();
	for each(var scholarResult:ScholarResult in uniqueResults)
	{
		if(reducedResults[scholarResult.year])
		{
			var yearObject:Object = reducedResults[scholarResult.year];
			yearObject.totalCount = yearObject.totalCount + 1;
			yearObject.highImpactCount = scholarResult.highImpactJournalsCount > 0 ?
				yearObject.highImpactCount + 1 : yearObject.highImpactCount;
		}
		else
		{
			reducedResults[scholarResult.year] = {totalCount: 1, highImpactCount: scholarResult.highImpactJournalsCount > 0 ? 1 : 0}
		}
		
	}
	
	// For every year and plasmid record, lets store the results
	// TODO: Create new table for high impact journals for every plasmid record ( to see the exact journals)
	for (var key:Object in reducedResults)
	{
		var finalResult:Object = reducedResults[key];
		var year:String = String(key);
		
		LoggerManager.logInfo(plasmidCode + " : " + finalResult.totalCount + " : " + finalResult.highImpactCount + " : " + year);
		
		if(StringUtil.trim(year).length == 0)
		{
			// Empty year! 
			// TODO: understand this case!
			year = "NA";
		}
		
		// TODO: Test if we should wait the delete to finish and then insert!
		// Alternative is to "GET" and check the count after that either update or insert
		// remove crawler result if already exists
		sqlManager.executeQuery(Query.DELETE_CRAWLER.replace("$code",plasmidCode).replace("$year",year));
			
		// insert updated version
		sqlManager.insertCrawlerRecords(plasmidCode, finalResult.totalCount, finalResult.highImpactCount, year, null, onInsertFailure);
	}	
	
	// Now let's store all the detailed view information for each plasmid citation
	// We need to group by year, as the search is based by year, and the replacement will happen only for the searched years, not all. 
	// TODO: Store author, citations, journal, year for each citation of each plasmid code
	var groupedResults:Dictionary = new Dictionary();
	for each(var scholarRes:ScholarResult in uniqueResults)
	{
		// We must replace the empty year before grouping!! Otherwise one of groupings will replace the other
		if(StringUtil.trim(scholarRes.year).length == 0)
		{
			scholarRes.year = "NA";
		}
		
		if(groupedResults[scholarRes.year])
		{
			groupedResults[scholarRes.year].push(scholarRes);
		}
		else
		{
			var resultsArray:Array = new Array();
			groupedResults[scholarRes.year] = resultsArray;
			groupedResults[scholarRes.year].push(scholarRes);
		}
	}
	
	for (var yearKey:Object in groupedResults)
	{
		var yearResults:Array = groupedResults[yearKey];
		
		sqlManager.executeQuery(Query.DELETE_DETAILEDVIEW.replace("$code",plasmidCode).replace("$year",String(yearKey)));
		
		for each(var result:ScholarResult in yearResults)
		{
			if(StringUtil.trim(String(result.year)).length == 0)
			{
				sqlManager.insertDetailedViewRecords(plasmidCode, "NA", result.journal, result.authors, String(result.citations), result.isCited ? '1' : '0', result.link, null, onInsertFailure);
			} else {
				sqlManager.insertDetailedViewRecords(plasmidCode, String(result.year), result.journal, result.authors, String(result.citations), result.isCited ? '1' : '0', result.link, null, onInsertFailure);
			}
		}
	}	
}

private function onInsertFailure(event:SQLErrorEvent):void
{
	LoggerManager.logError("onInsertFailure: Error inserting into the Database : " + event.text);
	Alert.show("Error inserting into the Database");
}

// Utility methods
/**
 * Opens google scholar to handle CAPTCHA case
 */
public function showBrowser():void
{
	var ttlWndw:ScholarBrowser = PopUpManager.createPopUp(this, ScholarBrowser, true) as ScholarBrowser;
	PopUpManager.centerPopUp(ttlWndw);
}

/**
 * Displays configuration dialog
 * Allows configuring high impact journals, 
 * search criteria..etc
 */
private function showConfigurationsDialog():void
{
	var configurationsDialog:ConfigurationDialog = PopUpManager.createPopUp(this, ConfigurationDialog, true) as ConfigurationDialog;
	PopUpManager.centerPopUp(configurationsDialog);
}

/**
 * Generic error handler
 */
private function onGenericError(event:Event=null):void
{
	// In case search stopped in the middle, we want to save what we got so far
	if(searchManager)
	{
		LoggerManager.logError("Error occured, attempting to recover any crawled results...");
		savePlasmidResults(searchManager.plasmidCode, searchManager.listUniqueResults[searchManager.plasmidCode], false)
	}
	
	stopSearching();
	
	// Parse and show error message
	var foundError:Boolean = false;
	if(event)
	{
		if(event is FaultEvent)
		{
			var fault:FaultEvent = FaultEvent(event);
			var msg:String = String(fault.fault.content);
			
			LoggerManager.logError("onGenericError: " + msg); 
			
			if(msg && msg.search('id="captcha"') > -1)
			{
				// It's a captcha error
				showBrowser();
			}
			else
			{
				Alert.show(msg, "Error");
			}
		}
	}
	
	if(!foundError)
	{
		LoggerManager.logError("onGenericError: " + genericError); 
		Alert.show(genericError, "Error");
	}
	status = genericError;
}
