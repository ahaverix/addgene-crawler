package managers.log
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.formatters.DateFormatter;
	

	public class LoggerManager
	{
		public static function logInfo(info:String):void
		{
			var formatter:DateFormatter = new DateFormatter();
			formatter.formatString = "YYYY/MM/DD at HH:NN:QQQ"
			
			var file:File = File.applicationStorageDirectory.resolvePath("web-crawler_info.log");
			
			var fileMode:String = (!file.exists ? FileMode.WRITE:FileMode.APPEND);
			
			try {
				var fileStream:FileStream = new FileStream();
				fileStream.open(file, fileMode);
				
				var logMessage:String = formatter.format(new Date()) + " : " + info + "\n";
				trace(logMessage);
				
				fileStream.writeUTFBytes(logMessage);
				fileStream.close();
			} catch (e:Error){
				// This case cannot happen at MAC
				// File seems to be in use..lets catch this silently without causing any crashes
				trace("File seems to be in use..lets catch this silently without causing any crashes");
			}
		}
		
		public static function logError(error:String):void
		{
			var formatter:DateFormatter = new DateFormatter();
			formatter.formatString = "YYYY/MM/DD at HH:NN:QQQ"
			
			var file:File = File.applicationDirectory.resolvePath("web-crawler_error.log");
			var fileMode:String = (!file.exists ? FileMode.WRITE:FileMode.APPEND);
			
			try{
				var fileStream:FileStream = new FileStream();
				fileStream.open(file, fileMode);
				
				var logMessage:String = formatter.format(new Date()) + " : " + error + "\n";
				trace(logMessage);
				
				fileStream.writeUTFBytes(logMessage);
				fileStream.close();
			} catch (e:Error){
				// This case cannot happen at MAC
				// File seems to be in use..lets catch this silently without causing any crashes
				trace("File seems to be in use..lets catch this silently without causing any crashes");
			}
			
			// We need the message to be also in the info file to maintain the flow
			logInfo(logMessage);
		}
	}
}