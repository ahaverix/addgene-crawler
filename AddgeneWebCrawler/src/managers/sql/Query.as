package managers.sql
{
	public class Query
	{
		/**
		 * Plasmids table
		 */
		// Create plasmid codes table
		public static var CREATE_PLASMIDS_TABLE:String = "CREATE TABLE IF NOT EXISTS plasmids (plasmid_id	INTEGER PRIMARY KEY AUTOINCREMENT,plasmid_code	TEXT UNIQUE,status	TEXT	DEFAULT '0',total_results	TEXT	DEFAULT '0')";
		
		// Loads all available plasmid ids
		public static var SELECT_PLASMIDS:String = "SELECT plasmid_id, plasmid_code, status, total_results FROM plasmids";
		
		// Loads all available plasmid ids
		public static var GET_CRAWLED_PLASMIDS_Count:String = "SELECT count(plasmid_id) as total FROM plasmids where status = '1'";
		
		// Loads all available plasmid ids
		public static var GET_ALL_CRAWLED_PLASMIDS:String = "SELECT plasmid_id, plasmid_code, status, total_results FROM plasmids where status = '1'";
			
		// Loads all available plasmid ids
		public static var SELECT_CRAWLED_PLASMID:String = "SELECT plasmid_id, plasmid_code, status, total_results FROM plasmids where plasmid_code = '$code' AND status = '1' AND total_results = '$total'";
			
		// Load by specific plasmid
		public static var FILTER_PLASMIDS:String = "SELECT plasmid_id, plasmid_code, status, total_results FROM plasmids WHERE plasmid_code LIKE '$code%'";
		
		// Insert: Replace '$code' with plasmid code
		public static var INSERT_NEW_PLASMIDS:String = "INSERT OR IGNORE INTO plasmids (plasmid_code) VALUES ('$code')";
		
		// Insert: Replace '$code' with plasmid code
		public static var INSERT_PLASMID:String = "INSERT OR IGNORE INTO plasmids (plasmid_code, status, total_results) VALUES ('$code', '$status', '$total')";
		
		// Delete single record from the table
		public static var DELETE_PLASMID:String = "DELETE FROM plasmids WHERE plasmid_code = '$code'";
		
		// Drop table
		public static var DROP_PLASMIDS:String = "DROP TABLE plasmids";
		
		// Update plasmid status
		public static var UPDATE_PLASMID_STATUS:String = "UPDATE plasmids SET status = '$status' where plasmid_code = '$code'";
			
		// Update plasmid status
		public static var UPDATE_PLASMID_TOTAL:String = "UPDATE plasmids SET total_results = '$total' where plasmid_code = '$code'";
		
		/////////////////////////////////////////////// UPDATE QUERIES /////////////////////////////////
		public static var PLASMIDS_ADD_STATUS_COLUMN:String = "ALTER TABLE plasmids ADD COLUMN status	TEXT	DEFAULT '0'";
		public static var PLASMIDS_ADD_TOTAL_COLUMN:String = "ALTER TABLE plasmids ADD COLUMN total_results	TEXT	DEFAULT '0'";
		
		
		/**
		 * Crawler table
		 */
		
		// Create crawler table
		public static var CREATE_CRAWLER_TABLE:String = "CREATE TABLE IF NOT EXISTS crawler (crawl_id	INTEGER PRIMARY KEY AUTOINCREMENT,plasmid_code	TEXT, " +
			"all_publications	TEXT,high_impact_journals	TEXT,year	TEXT)";
		
		// Loads all available crawler data
		public static var SELECT_CRAWLER:String = "SELECT crawl_id, plasmid_code, all_publications,high_impact_journals,year FROM crawler";
		
		// Loads distinct list of plasmid codes
		public static var SELECT_CRAWLED_PLASMIDS:String = "SELECT DISTINCT plasmid_code FROM crawler";
		
		// Load by specific plasmid
		public static var FILTER_CRAWLER:String = "SELECT crawl_id, plasmid_code, all_publications,high_impact_journals,year FROM crawler WHERE plasmid_code LIKE '$code%'";
		
		// Insert new crawler record
		public static var INSERT_NEW_CRAWLER:String = "INSERT OR IGNORE INTO crawler (plasmid_code,all_publications,high_impact_journals,year) VALUES ('$code',$allPublications,$highImpact,'$year')";
	
		// Update crawler record
		public static var UPDATE_CRAWLER:String = "INSERT OR IGNORE INTO crawler (plasmid_code,all_publications,high_impact_journals,year) VALUES ('$code',$allPublications,$highImpact,'$year') WHERE plasmid_code='$code' AND year='$year'";
		
		// Load by specific plasmid
		public static var GET_CRAWLER:String = "SELECT crawl_id, plasmid_code, all_publications,high_impact_journals,year FROM crawler WHERE plasmid_code = '$code' AND year = '$year'";
		
		// Delete crawler record from the table
		public static var DELETE_CRAWLER:String = "DELETE FROM crawler WHERE plasmid_code = '$code' AND year = '$year'";
		
		// Drop table
		public static var DROP_CRAWLER:String = "DROP TABLE crawler";
		
		/**
		 * Detailed view table
		 */
		
		// Create detailed view table
		public static var CREATE_DETAILEDVIEW_TABLE:String = "CREATE TABLE IF NOT EXISTS detailedView (detailed_id	INTEGER PRIMARY KEY AUTOINCREMENT,plasmid_code	TEXT, " +
			"year	TEXT,journal	TEXT,authors	TEXT,citations	TEXT,isFullCitation	TEXT	DEFAULT '0',key	TEXT	DEFAULT '')";
		
		// Loads all available detailed view data
		public static var SELECT_DETAILEDVIEW:String = "SELECT detailed_id, plasmid_code, year, journal, authors,citations,isFullCitation,key FROM detailedView";
		
		// Loads all available detailed view data
		public static var SELECT_DETAILEDVIEW_BY_KEY:String = "SELECT detailed_id, plasmid_code, year, journal, authors,citations,isFullCitation,key FROM detailedView WHERE key = '$key'";
		
		// Load by specific plasmid
		public static var FILTER_DETAILEDVIEW:String = "SELECT detailed_id, plasmid_code, year, journal, authors,citations,isFullCitation,key FROM detailedView WHERE plasmid_code LIKE '$code%'";
		
		// Insert new detailed record
		public static var INSERT_NEW_DETAILEDVIEW:String = "INSERT OR IGNORE INTO detailedView (plasmid_code,year,journal,authors,citations,isFullCitation,key) VALUES ('$code','$year','$journal','$authors','$citations', '$isFull','$key')";
		
		// Delete detailedView records from the table
		public static var DELETE_DETAILEDVIEW:String = "DELETE FROM detailedView WHERE plasmid_code = '$code' AND year = '$year'";
		
		// Drop table
		public static var DROP_DETAILEDVIEW:String = "DROP TABLE detailedView";
		/////////////////////////////////////////////// UPDATE QUERIES /////////////////////////////////
		public static var DETAILS_ADD_FULL_CITATION:String = "ALTER TABLE detailedView ADD COLUMN isFullCitation	TEXT	DEFAULT '0'";
		public static var DETAILS_ADD_CITATION_KEY:String = "ALTER TABLE detailedView ADD COLUMN key	TEXT	DEFAULT ''";
	}
}