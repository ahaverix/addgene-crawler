package managers.sql
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.EventDispatcher;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.utils.setTimeout;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.rpc.Fault;
	import mx.utils.StringUtil;
	
	import managers.sql.events.SQLConnectorEvent;
	import managers.sql.events.SQLManagerEvent;
	
	public class SQLManager
	{
		// Class variables
		[Bindable]
		public var connector:SQLConnector;
		
		// Contains all the plasmid ids in the system
		[bindable]
		public var plasmids:ArrayCollection;
		[Bindable]
		public var crawledPlasmidsCount:Number = 0;
		
		public var allPlasmids:ArrayCollection;
		
		// Contains all the crawler data in the system
		[bindable]
		public var crawler:ArrayCollection;
		
		// Contains all the detailedView data in the system
		[bindable]
		public var detailedView:ArrayCollection;
		
		// Class dispatcher
		public var dispatcher:EventDispatcher;
		
		// Initializers
		public function SQLManager(sqlConnector:SQLConnector=null)
		{
			dispatcher = new EventDispatcher();
			plasmids = new ArrayCollection();
			allPlasmids = new ArrayCollection();
			crawler = new ArrayCollection();
			detailedView = new ArrayCollection();
			
			if(!sqlConnector)
			{
				connector = new SQLConnector();
				connector.addConnectedEventListener(onConnectionSuccessful);
				connector.addErrorEventListener(onConnectionFailure);
				connector.connect();
			}
			else
			{
				connector = sqlConnector;
			}
		}
		
		private function onConnectionSuccessful(event:SQLConnectorEvent):void 
		{
			// Create tables if doesn't exists
			executeQuery(Query.CREATE_PLASMIDS_TABLE);
			executeQuery(Query.CREATE_CRAWLER_TABLE);
			executeQuery(Query.CREATE_DETAILEDVIEW_TABLE);
			
			// Perform any database schema updates
			setTimeout(SQLUpdater.updateTables, 300, this);
			
			// Load all data
			setTimeout(loadAll, 600);
		}
		
		private function onConnectionFailure(event:SQLConnectorEvent):void 
		{
			Alert.show(event.toString(), "Error connecting to Database");
		}
		
		// SQL Statements methods
		public function executeQuery(query:String, resultHandler:Function=null, faultHandler:Function=null):void
		{
			var createStmt:SQLStatement = new SQLStatement();
			createStmt.sqlConnection = connector.getSqlConnection();
			createStmt.text = query;
			
			if(resultHandler != null)
				createStmt.addEventListener(SQLEvent.RESULT, resultHandler);
			
			if(faultHandler != null)
				createStmt.addEventListener(SQLErrorEvent.ERROR, faultHandler);
			
			createStmt.execute();
		}
		
		// Method to insert new plasmid code
		public function insertPlasmidId(plasmidId:String, resultHandler:Function=null, faultHandler:Function=null):void
		{
			var createStmt:SQLStatement = new SQLStatement();
			createStmt.sqlConnection = connector.getSqlConnection();
			createStmt.text = Query.INSERT_NEW_PLASMIDS.replace("$code", plasmidId).replace('$status', "0").replace('$total', "0");			
			if(resultHandler != null)
				createStmt.addEventListener(SQLEvent.RESULT, resultHandler);
			
			if(faultHandler != null)
				createStmt.addEventListener(SQLErrorEvent.ERROR, faultHandler);
			
			createStmt.execute();
		}
		
		public function insertPlasmid(plasmidId:String, plasmidStatus:String, total:String,resultHandler:Function=null, faultHandler:Function=null):void
		{
			var createStmt:SQLStatement = new SQLStatement();
			createStmt.sqlConnection = connector.getSqlConnection();
			createStmt.text = Query.INSERT_PLASMID.replace("$code", plasmidId).replace('$status', plasmidStatus).replace('$total', total);
			
			if(resultHandler != null)
				createStmt.addEventListener(SQLEvent.RESULT, resultHandler);
			
			if(faultHandler != null)
				createStmt.addEventListener(SQLErrorEvent.ERROR, faultHandler);
			
			createStmt.execute();
		}
		
		public function insertCrawlerRecords(plasmidId:String, allPublicationsCount:Number, highImpactCount:Number, year:String
											 ,resultHandler:Function=null, faultHandler:Function=null):void
		{
			var createStmt:SQLStatement = new SQLStatement();
			createStmt.sqlConnection = connector.getSqlConnection();
			createStmt.text = Query.INSERT_NEW_CRAWLER.replace("$code", plasmidId).
				replace("$allPublications", allPublicationsCount.toString()).replace("$highImpact", highImpactCount.toString()).
				replace("$year", year);
			
			if(resultHandler != null)
				createStmt.addEventListener(SQLEvent.RESULT, resultHandler);
			
			if(faultHandler != null)
				createStmt.addEventListener(SQLErrorEvent.ERROR, faultHandler);
			
			createStmt.execute();
		}
		
		public function insertDetailedViewRecords(plasmidId:String, year:String, journal:String, authors:String, citations:String, isCited:String, key:String
											 ,resultHandler:Function=null, faultHandler:Function=null):void
		{
			var createStmt:SQLStatement = new SQLStatement();
			createStmt.sqlConnection = connector.getSqlConnection();
			createStmt.text = Query.INSERT_NEW_DETAILEDVIEW.replace("$code", plasmidId).
				replace("$year", year).replace("$journal", journal).replace("$authors", authors).replace("$citations", citations).replace("$isFull", isCited).replace("$key", key);
			
			if(resultHandler != null)
				createStmt.addEventListener(SQLEvent.RESULT, resultHandler);
			
			if(faultHandler != null)
				createStmt.addEventListener(SQLErrorEvent.ERROR, faultHandler);
			
			createStmt.execute();
		}
		
		// Utility methods
		public function loadAll():void
		{
			// Notify subscribers manager is ready
			dispatcher.dispatchEvent(new SQLManagerEvent(SQLManagerEvent.READY));
			
			loadAllPlasmids();
			loadAllResults();
			loadAllDetailedResults();
		}
		
		public function loadAllPlasmids():void
		{
			executeQuery(Query.SELECT_PLASMIDS,onPlasmidsLoaded);
		}
		
		public function loadAllResults():void
		{
			executeQuery(Query.SELECT_CRAWLER,onCrawlersLoaded);
		}
		
		public function loadAllDetailedResults():void
		{
			executeQuery(Query.SELECT_DETAILEDVIEW,onDetailedViewsLoaded);
		}
		
		public function onPlasmidsLoaded(event:SQLEvent):void
		{
			var result:SQLResult = (event.currentTarget as SQLStatement).getResult();
			plasmids.source = result.data;
			allPlasmids.source = result.data;
			
			dispatcher.dispatchEvent(new SQLManagerEvent(SQLManagerEvent.PLASMIDS_LOADED));
			
			// Lets check how many plasmids are successfully crawled
			executeQuery(Query.GET_CRAWLED_PLASMIDS_Count, function(event:SQLEvent):void
			{
				crawledPlasmidsCount = 0;
				var result:SQLResult = (event.currentTarget as SQLStatement).getResult();
				
				if(result.data && result.data.length == 1 && !isNaN(Number(result.data[0].total)))
					crawledPlasmidsCount = Number(result.data[0].total);
			});
		}
		
		public function onPlasmidsFiltered(event:SQLEvent):void
		{
			var result:SQLResult = (event.currentTarget as SQLStatement).getResult();
			plasmids.source = result.data;
			
			dispatcher.dispatchEvent(new SQLManagerEvent(SQLManagerEvent.PLASMIDS_LOADED));
		}
		
		public function onCrawlersLoaded(event:SQLEvent):void
		{
			var result:SQLResult = (event.currentTarget as SQLStatement).getResult();
			crawler.source = result.data;
			
			dispatcher.dispatchEvent(new SQLManagerEvent(SQLManagerEvent.CRAWLER_RESULTS_LOADED));
		}
		
		public function onDetailedViewsLoaded(event:SQLEvent):void
		{
			var result:SQLResult = (event.currentTarget as SQLStatement).getResult();
			detailedView.source = result.data;
			
			dispatcher.dispatchEvent(new SQLManagerEvent(SQLManagerEvent.DETAILED_VIEWS_LOADED));
		}
		
		/**
		 * Filter results depending on filter keyword
		 */
		public function filterResultsByKeyword(filterKeyword:String, resultHandler:Function, faultHandler:Function):void
		{
			var query:String = Query.SELECT_CRAWLER;
			if(filterKeyword.length > 0)
				query = Query.FILTER_CRAWLER.replace("$code",filterKeyword);
			
			executeQuery(query, resultHandler, faultHandler);
		}
		
		/**
		 * Filter Plasmid codes depending on filter keyword
		 */
		public function filterPlasmidsByKeyword(filterKeyword:String, resultHandler:Function, faultHandler:Function):void
		{
			var query:String = Query.SELECT_PLASMIDS;
			if(filterKeyword.length > 0)
				query = Query.FILTER_PLASMIDS.replace("$code",filterKeyword);
			
			executeQuery(query, resultHandler, faultHandler);
		}
		
		/**
		 * Filter detailed records depending on filter keyword
		 */
		public function filterDetailedViewsByKeyword(filterKeyword:String, resultHandler:Function, faultHandler:Function):void
		{
			var query:String = Query.SELECT_DETAILEDVIEW;
			if(filterKeyword.length > 0)
				query = Query.FILTER_DETAILEDVIEW.replace("$code",filterKeyword);
			
			executeQuery(query, resultHandler, faultHandler);
		}
	}
}