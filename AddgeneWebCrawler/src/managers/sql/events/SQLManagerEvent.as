package managers.sql.events
{
	import flash.events.Event;
	
	public class SQLManagerEvent extends Event
	{
		public static var PLASMIDS_LOADED:String = "all_plasmids_loaded";
		public static var JOURNALS_LOADED:String = "all_journals_loaded";
		public static var CRAWLER_RESULTS_LOADED:String = "all_crawler_results_loaded";
		public static var DETAILED_VIEWS_LOADED:String = "all_detailed_views_loaded";
		public static var READY:String = "sql_manager_ready";
		
		public function SQLManagerEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}