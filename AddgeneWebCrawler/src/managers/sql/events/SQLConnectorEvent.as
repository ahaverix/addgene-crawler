package managers.sql.events
{
	import flash.events.Event;
	
	public class SQLConnectorEvent extends Event
	{
		public static var CONNECTED:String = "sqlConnectionSuccessful";
		public static var ERROR:String = "sqlConnectionError";
		
		public function SQLConnectorEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}