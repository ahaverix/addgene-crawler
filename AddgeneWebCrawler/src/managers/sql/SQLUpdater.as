package managers.sql
{
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;

	public class SQLUpdater
	{
		/**
		 * Since we distributed the application, we need to update the database properly
		 * instead of deleting the database and create new one
		 */
		public static function updateTables(sqlManager:SQLManager):void
		{
			updatePlasmidsTable(sqlManager);
			updateCrawlingTable(sqlManager);
			updateDetailsTable(sqlManager);
		}
		
		public static function updatePlasmidsTable(sqlManager:SQLManager):void
		{
			sqlManager.executeQuery(Query.SELECT_PLASMIDS, function(evt:SQLEvent):void
			{
				// Status column exists
			}, function(error:SQLErrorEvent):void
			{
				if(error.error.details == "no such column: 'status'" || error.error.details == "no such column: 'total_results'")
				{
					// Status column doesn't exist
					sqlManager.executeQuery(Query.PLASMIDS_ADD_STATUS_COLUMN);
					sqlManager.executeQuery(Query.PLASMIDS_ADD_TOTAL_COLUMN);
					sqlManager.executeQuery(Query.DETAILS_ADD_FULL_CITATION);
					sqlManager.executeQuery(Query.DETAILS_ADD_CITATION_KEY);
				}
			});
		}
		
		public static function updateCrawlingTable(sqlManager:SQLManager):void
		{
			
		}
		
		public static function updateDetailsTable(sqlManager:SQLManager):void
		{
			
		}
	}
}