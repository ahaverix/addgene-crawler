package managers.sql
{
	import flash.data.SQLConnection;
	import flash.events.EventDispatcher;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	
	import managers.sql.events.SQLConnectorEvent;

	public class SQLConnector
	{
		// Class variables
		private var databaseFileName:String = ""
		private var databaseFile:File;
		private var sqlConn:SQLConnection;
		private var connectionEstablished:Boolean = false;
		private var dispatcher:EventDispatcher = new EventDispatcher();
		
		[Bindable]
		public function getDatabaseLocation():String 
		{
			return databaseFile.nativePath;
		}
		
		[Bindable]
		public function getDatabaseFileName():String
		{
			return databaseFileName;
		}
		
		public function getDatabaseDirectory():File 
		{
			return databaseFile.parent;
		}
		
		public function getSqlConnection():SQLConnection
		{
			return sqlConn;
		}
		
		// Subscribe to class events
		public function addConnectedEventListener(listener:Function):void 
		{
			dispatcher.addEventListener(SQLConnectorEvent.CONNECTED, listener);
		}
		
		public function addErrorEventListener(listener:Function):void 
		{
			dispatcher.addEventListener(SQLConnectorEvent.ERROR, listener);
		}
		
		// Init
		public function SQLConnector(dbFileName:String="webcrawler.db", dbLocation:File=null)
		{	
			// If not specific location, default to desktop
			dbLocation = !dbLocation ? File.applicationStorageDirectory : dbLocation;
			
			// Initialize properties
			databaseFileName = dbFileName;
			databaseFile = dbLocation.resolvePath(databaseFileName);
		}
		
		public function connect():void
		{
			sqlConn = new SQLConnection();
			sqlConn.addEventListener(SQLEvent.OPEN, onConnectionOpened);
			sqlConn.addEventListener(SQLErrorEvent.ERROR, onConnectionFailure);
			sqlConn.openAsync(databaseFile);
		}
		
		// On Connection sucessful, set status and notify listeners
		private function onConnectionOpened(event:SQLEvent):void 
		{
			connectionEstablished = true;
			dispatcher.dispatchEvent(new SQLConnectorEvent(SQLConnectorEvent.CONNECTED));
		}
		
		// On Connection failure, set status and notify listeners
		private function onConnectionFailure(event:SQLErrorEvent):void 
		{
			connectionEstablished = false;
			dispatcher.dispatchEvent(new SQLConnectorEvent(SQLConnectorEvent.ERROR));
		}
	}
}