package managers.configuration
{
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.setTimeout;
	
	import managers.log.LoggerManager;
	
	import models.Configuration;

	
	public class ConfigurationsManager
	{
		// Static instance
		public static var sharedManager:ConfigurationsManager = new ConfigurationsManager();
		
		// Instance of the configuration file available on disk
		private const configurationFileName:String = "confs.txt";
		private var configurationFile:File;
		private var configurationFileContent:String = "";
		public var onLoad:Function;
		public var onError:Function;
		
		[Bindable]
		public var confs:Configuration = new Configuration();
		
		// Init
		public function ConfigurationsManager(onload:Function=null, onError:Function=null)
		{
			this.onLoad = onload;
			this.onError = onError;
			
			configurationFile = File.applicationStorageDirectory.resolvePath(configurationFileName);
			load();
		}
		
		public function load():void
		{
			var fileLoader:URLLoader = new URLLoader();
			fileLoader.addEventListener(Event.COMPLETE, onConfigurationsLoaded);
			fileLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoadError);
			
			LoggerManager.logInfo(configurationFile.url);
			fileLoader.load(new URLRequest(configurationFile.url));
		}
		
		private function onConfigurationsLoaded(event:Event):void
		{
			var fileContent:String = String((event.currentTarget as URLLoader).data);
			
			confs = new Configuration(fileContent);
			LoggerManager.logInfo(fileContent);
			
			if(onLoad)
				setTimeout(onLoad, 0);
		}
		
		private function onLoadError(event:IOErrorEvent):void
		{
			// Seems file doesn't exist
			ConfigurationsManager.writeToFile("", configurationFile);
		}
		
		public function save():void
		{
			ConfigurationsManager.writeToFile(confs.content, configurationFile);
		}
		
		/**
		 * Utility method to write content to a file
		 */
		public static function writeToFile(fileContent:String, file:File):void
		{
			var stream:FileStream = new FileStream();
			try{
				stream.open(file, FileMode.WRITE);
				stream.writeUTFBytes(fileContent);
				stream.close();
			}catch(e:Error){}
		}
	}
}