package managers.services
{
	public interface ICrawlerService extends IRemoteProxy 
	{
		// This will search scholar for specific plasmid keyword
		function searchScholarByKeyword(params:Object, resultHandler:Function, faultHandler:Function):void;
		
		function getResultCitation(citationKey:String, citationNumber:String, resultHandler:Function, faultHandler:Function):void;
	}
}