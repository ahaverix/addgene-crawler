package managers.services
{
	import mx.rpc.AsyncToken;

	public interface IRemoteProxy 
	{
		function get(url:String, parameters:Object, resultHandler:Object, faultHandler:Object):void;
		function post(url:String, parameters:Object, resultHandler:Object, faultHandler:Object):AsyncToken;
	}
}