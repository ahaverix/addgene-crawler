package managers.services.impl
{
	import flash.events.IEventDispatcher;
	
	import managers.services.ICrawlerService;
	
	public class GoogleScholarServices extends RemoteProxy implements ICrawlerService
	{
		// Initializers
		public function GoogleScholarServices(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function searchScholarByKeyword(params:Object, resultHandler:Function, faultHandler:Function):void
		{
			var url:String = "https://scholar.google.com/scholar";
			get(url, params, resultHandler, faultHandler);
		}
		
		public function getResultCitation(citationKey:String, citationNumber:String, resultHandler:Function, faultHandler:Function):void
		{
			var url:String = "https://scholar.google.com/scholar?q=info:" + citationKey +":scholar.google.com/&output=cite&scirp="+ citationNumber +"&hl=en";
			get(url, null, resultHandler, faultHandler);
		}
	}
}