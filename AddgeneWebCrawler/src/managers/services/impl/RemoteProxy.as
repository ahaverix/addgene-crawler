package managers.services.impl
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestDefaults;
	import flash.net.URLVariables;
	import flash.utils.setTimeout;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.Fault;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	
	import managers.log.LoggerManager;
	import managers.services.IRemoteProxy;
	
	public class RemoteProxy extends EventDispatcher implements IRemoteProxy
	{
		// Initializers
		public function RemoteProxy(target:IEventDispatcher=null)
		{
			super(target);
			URLRequestDefaults.manageCookies = true;
			URLRequestDefaults.useCache = true;
			URLRequestDefaults.followRedirects = true;
		}
		
		// Utility methods
		public function get(url:String, parameters:Object, resultHandler:Object, faultHandler:Object):void
		{
			LoggerManager.logInfo(url);
			
			var loader:URLLoader = new URLLoader();
			var request:URLRequest = new URLRequest(url);
			
			var variables:URLVariables = new URLVariables();
			
			if(parameters)
			{
				for(var id:String in parameters) {
					var value:Object = parameters[id];
					variables[id]=value;
				}
			}
			
			request.data = variables;
			request.method = "GET";
			
			request.url = url;
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			
			loader.addEventListener(Event.COMPLETE, function oncomplete(evt:Event):void
			{
				LoggerManager.logInfo("Returned successfully...");
				
				var resultEvt:ResultEvent = new ResultEvent(ResultEvent.RESULT, false, true, loader.data);
				
				setTimeout((resultHandler as Function),0, resultEvt);
			});
			
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onError(evt:IOErrorEvent):void
			{
				var  errorMessage:String = loader.data as String;
				var faultMsg:String = "IO_ERROR " + errorMessage;
				
				LoggerManager.logInfo("ERROR happened: " + faultMsg);
				
				var serviceFault:Fault = new Fault(faultMsg,faultMsg,faultMsg);
				var faultEvt:FaultEvent = new FaultEvent(FaultEvent.FAULT, false, true, serviceFault);
				faultEvt.fault.content = faultMsg;
				
				setTimeout((faultHandler as Function), 0, faultEvt);
			}); 
			
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, function onError(evt:SecurityErrorEvent):void
			{
				var  errorMessage:String = loader.data as String;
				var faultMsg:String = "SECURITY_ERROR " + errorMessage;
				
				LoggerManager.logInfo("ERROR happened: " + faultMsg);
				
				var serviceFault:Fault = new Fault(faultMsg,faultMsg,faultMsg);
				var faultEvt:FaultEvent = new FaultEvent(FaultEvent.FAULT, false, true, serviceFault);
				faultEvt.fault.content = faultMsg;
				
				setTimeout((faultHandler as Function), 0, faultEvt);
			}); 
			
			loader.load(request);
		}
		
		public function post(url:String, parameters:Object, resultHandler:Object, faultHandler:Object):AsyncToken
		{
			var service:HTTPService = new HTTPService();
			service.url = url;
			service.resultFormat = "e4x";
			service.method = "POST";
			service.headers = {"Accept-Encoding":"gzip","User-Agent":"gzip"};
			
			URLRequestDefaults.idleTimeout = 100000000;
			
			if(parameters)
				service.request = parameters;
			else
				service.contentType = "application/xml";
			
			if(resultHandler is Array)
			{
				for each(var handler:Function in resultHandler)
				{
					if(handler != null && handler is Function)
						service.addEventListener(ResultEvent.RESULT, handler, false, 0, true);
				}
			}
			else if(resultHandler is Function)
				service.addEventListener(ResultEvent.RESULT, resultHandler as Function, false, 0, true);
			
			if(faultHandler is Array)
			{
				for each(handler in faultHandler)
				{
					if(handler != null && handler is Function)
						service.addEventListener(FaultEvent.FAULT, handler, false, 0, true);
				}
			}
			else if(faultHandler is Function)
				service.addEventListener(FaultEvent.FAULT, faultHandler as Function, false, 0, true);
			
			return service.send();
		}
	}
}