package managers.parsers
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.utils.Dictionary;
	import flash.utils.setTimeout;
	
	import mx.collections.ArrayCollection;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;
	
	import events.ScholarResultEvent;
	
	import managers.configuration.ConfigurationsManager;
	import managers.log.LoggerManager;
	import managers.parsers.events.SearchManagerEvent;
	import managers.services.ICrawlerService;
	import managers.services.impl.GoogleScholarServices;
	import managers.sql.Query;
	import managers.sql.SQLManager;
	
	import models.ParseVariables;
	import models.ScholarResult;
	import models.SearchParameters;

	/**
	 * This class will take care of searching scholar
	 * Should be responsible for searching and parsing all the 
	 * possibilities for specific plasmid
	 * - Keyword Possibilities
	 * - Years possibilities
	 */
	public class PlasmidSearchManager
	{
		// TODO: make those configurable (use numbers and range instead of static text)
		public var requestDelay:Number = ConfigurationsManager.sharedManager.confs.Delay;
		public function get searchYears():ArrayCollection {
			var tmpArray:Array = [];
			
			for(var i:Number= 1920; i<=(new Date()).fullYear; i++)
			{
				tmpArray.push(i);
			}
				
			return new ArrayCollection(tmpArray);
		}
		//private var searchYears:Array = ["2016"];
		
		// TODO: load possibilities from configurable place
		private var possibilities:Array = ['"plasmid $code"','"plasmid* $code"','"plasmid** $code"','"plasmid*** $code"',
			'"addgene $code"','"addgene* $code"','"addgene** $code"','"addgene*** $code"'];
		
		private var journalKeywords:Array = ["Biochemical", "Biochemical Journal", "Biological Chemistry", "Journal of Biological Chemistry",
			"ACS", "ACS Synthetic Biology", "ACS Chemical Biology", "Scientific Reports", "Nucleic Acids Research", "PNAS", "Proceedings of the National Academy of Sciences", "EMBO", "EMBO Journal", "Journal of the American Chemical Society", "American Chemical Society", 
			"Cell", "Science", "Nature Reviews Genetics", "Nature Reviews Molecular Cell Biology", "Molecular Cell Biology", "Nature", "Nature Biotechnology"];
		
		// Web-service used for performing search
		private var crawlerService:ICrawlerService;
		public var plasmidCode:String = "";
		
		// Plasmid search variables
		public var uniqueResults:Dictionary;
		private var resultHandler:Function;
		private var faultHandler:Function;
		
		// Page parsing variables
		public var currentPossibilityTotalCount:Number;
		private var currentPossibilityTotalPages:Number;
		private var currentPossibilityCurrentPage:Number;
		private var currentPossibilityHasErrors:Boolean;
		
		// Class dispatcher
		public var dispatcher:EventDispatcher;
		
		// List search variables
		public var currentPlasmidIndex:uint;
		private var plasmidCodes:Array;
		public var listUniqueResults:Dictionary;
		private var listResultHandler:Function;
		private var listFaultHandler:Function;
		private var sqlManager:SQLManager;
		private var searchAborted:Boolean = false;
		
		// Init
		public function PlasmidSearchManager(sqlManager:SQLManager)
		{
			this.sqlManager = sqlManager;
			
			// Initialize search service
			crawlerService = new GoogleScholarServices();
			
			dispatcher = new EventDispatcher();
		}
		
		public function abort():void
		{
			searchAborted = true;
		}
		
		
		/**
		 * Drive the search for list of plasmids
		 * Return prepares a dictionary for all unique results
		 */
		public function searchPlasmidsList(plasmidCodes:Array, onSearchComplete:Function, onError:Function):void
		{
			if(plasmidCodes.length > 0)
			{
				searchAborted = false;
				this.plasmidCode = "";
				currentPlasmidIndex = 0;
				this.plasmidCodes = plasmidCodes;
				listUniqueResults = new Dictionary();
				listResultHandler = onSearchComplete;
				listFaultHandler = onError;
				
				// Lets search the 1st plasmid
				searchByPlasmidIndex(currentPlasmidIndex, onSearchComplete, onError);
			}
		}
		
		private function searchByPlasmidIndex(index:uint, onSearchComplete:Function, onError:Function):void
		{
			// Notify on new search progress..
			dispatcher.dispatchEvent(new SearchManagerEvent(SearchManagerEvent.PROGRESS));
			
			searchByPlasmidCode(plasmidCodes[index], onListSearchComplete, onListSearchError);
		}
		
		private function onListSearchComplete():void
		{
			// Plasmid search done successfully
			// Lets store the result
			listUniqueResults[plasmidCodes[currentPlasmidIndex]] = uniqueResults;
			
			// Continue next
			continueListSearching();
		}
		
		private function onListSearchError(event:Event=null):void
		{
			// Plasmid search stopped unexpectedly!!
			// Lets store what we got so far (can he useful)
			
			listUniqueResults[plasmidCodes[currentPlasmidIndex]] = uniqueResults;
			
			setTimeout(listFaultHandler, 100, event);
		}
		
		private function continueListSearching(isSkip:Boolean=false):void
		{
			if(currentPlasmidIndex == this.plasmidCodes.length - 1)
			{
				// Search is done! Notify...
				if(!isSkip)
					dispatcher.dispatchEvent(new SearchManagerEvent(SearchManagerEvent.PROGRESS, this.plasmidCode, listUniqueResults[this.plasmidCode], currentPossibilityHasErrors));
				
				setTimeout(listResultHandler, 100);
			}
			else
			{
				// Notify on new search progress..
				if(!isSkip)
					dispatcher.dispatchEvent(new SearchManagerEvent(SearchManagerEvent.PROGRESS, this.plasmidCode, listUniqueResults[this.plasmidCode], currentPossibilityHasErrors));
				
				currentPlasmidIndex ++;
				setTimeout(searchByPlasmidIndex, randomRange(requestDelay/2, requestDelay*3/2), currentPlasmidIndex, onListSearchComplete, onListSearchError);
			}
		}
		
		/**
		 * Perform searching for all the available possibilities
		 * Search by all keywords possibility
		 * Search by all years possibilities
		 * 
		 * @param plasmidCode = Plasmid code
		 * @param onSearchComplete = Function will be triggered after successful search
		 * @param onError = Function will be triggered if search failed
		 */
		public function searchByPlasmidCode(plasmidCode:String, onSearchComplete:Function, onError:Function):void
		{
			// Set variables
			this.plasmidCode = plasmidCode;
			this.resultHandler = onSearchComplete;
			this.faultHandler = onError;
			
			// Dictionary for caching results
			uniqueResults = new Dictionary();
			
			// Reset varibles
			resetVariables();
			
			// New search, should start from the begining (Zero count)
			performSearch(0, plasmidCode);
		}
		
		// Reset variables for every possibility
		private function resetVariables():void
		{
			currentPossibilityTotalCount = 0;
			currentPossibilityTotalPages = 0;
			currentPossibilityCurrentPage = 0;
			currentPossibilityHasErrors = false;
			currentlyParsingResults = new Array();
		}
		
		/**
		 * Performs the actual search
		 * Takes start results as parameter
		 * Will be used for search first or later pages
		 */
		private function performSearch(startFrom:Number, plasmidCode:String):void
		{
			var searchKeyword:String = ConfigurationsManager.sharedManager.confs.keywords.join(" | ");
			
			// Special case for plasmids less than 1000 or known years
			// Check if code is a number and less than 1000 OR if its a known year
			var numberPlasmidCode:Number = Number(this.plasmidCode.replace(",","").replace(".",""));
			if(!isNaN(numberPlasmidCode) && (numberPlasmidCode < 1000 || searchYears.contains(numberPlasmidCode.toString()))
			&& ConfigurationsManager.sharedManager.confs.keywords.length > 3)
			{
				var tmpKeywords:Array = [];
				for(var i:uint=0; i< ConfigurationsManager.sharedManager.confs.keywords.length; i++) 
				{
					if(String(ConfigurationsManager.sharedManager.confs.keywords[i]) != '"plasmid** $code"' &&
						String(ConfigurationsManager.sharedManager.confs.keywords[i]) != '"plasmid*** $code"' && 
						String(ConfigurationsManager.sharedManager.confs.keywords[i]) != '"addgene** $code"' && 
						String(ConfigurationsManager.sharedManager.confs.keywords[i]) != '"addgene*** $code"')
					{
						tmpKeywords.push(String(ConfigurationsManager.sharedManager.confs.keywords[i]));
					}
				}
				
				if(tmpKeywords.length > 0)
					searchKeyword = tmpKeywords.join(" | ");
			} 
			
			
			while(searchKeyword.indexOf("$code") > -1)
			{
				searchKeyword = searchKeyword.replace("$code",plasmidCode);
			}
			
			LoggerManager.logInfo("Searching: " + searchKeyword);
			
			var params:Object = getParams(searchKeyword, startFrom);
			
			crawlerService.searchScholarByKeyword(params, onSearch, onGenericError);
		}
		
		private function onSearch(event:ResultEvent):void
		{
			// Parse google response
			var responseText:String = String(event.result);
			
			if(currentPossibilityTotalPages == 0)
			{
				// This means its the first page to parse
				// Lets parse the totals
				if(!parseTotals(responseText))
				{
					// Don't continue searching
					return;
				}
				
				LoggerManager.logInfo("First page results, lets check if this search already saved...")
				checkIfAlreadyExists(responseText);
			}
			else
			{
				// Parse page results
				parseResults(responseText);
			}
		}
		
		private function checkIfAlreadyExists(responseText:String):void
		{
			sqlManager.executeQuery(Query.SELECT_CRAWLED_PLASMID.replace("$code", this.plasmidCode).replace('$total', currentPossibilityTotalCount.toString()), function(result:SQLEvent):void
			{
				var records:SQLResult = (result.currentTarget as SQLStatement).getResult();
				
				if(records && records.data && records.data.length == 1)
				{
					LoggerManager.logInfo("Search found in the database, attempting to skip..")
					// Continue next
					continueListSearching(true);
					return;
				}
				
				// Parse page results
				parseResults(responseText);
				
			}, function(evt:SQLErrorEvent):void
			{
				// Parse page results
				parseResults(responseText);
			});
		}
		
		private function parseTotals(responseText:String):Boolean
		{
			LoggerManager.logInfo("Parsing search results...")
			// Total results
			var totalsArray:Array = responseText.split(ParseVariables.TOTAL_RESULTS);
			
			if(totalsArray.length > 1)
			{
				// Search is successful
				var totalResults:String = totalsArray[1];
				totalResults = totalResults.split(ParseVariables.TOTAL_RESULTS_CLEANUP)[0];
				totalResults = StringUtil.trim(totalResults);
				
				if(totalResults.split(" ").length > 1)
					totalResults = StringUtil.trim(totalResults.split(" ")[1]);
				
				// This is the total number of results (for all pages!!)
				LoggerManager.logInfo(totalResults);
				
				currentPossibilityTotalCount = Number(totalResults.replace(",","").replace(".",""));
				
				if(!isNaN(currentPossibilityTotalCount) && currentPossibilityTotalCount > 0)
				{
					// Calculate total pages
					currentPossibilityTotalPages = Math.ceil(currentPossibilityTotalCount / 20);
					currentPossibilityCurrentPage = 1;
					
					LoggerManager.logInfo("Total pages: " + currentPossibilityTotalPages.toString());
					
					// Check if code is a number and less than 1000 OR if its a known year
					var numberPlasmidCode:Number = Number(this.plasmidCode.replace(",","").replace(".",""));
					if(!isNaN(numberPlasmidCode) && numberPlasmidCode < 1000) 
					{
						LoggerManager.logInfo("Plasmid code is less than 1000: " + numberPlasmidCode.toString());
						
						currentPossibilityTotalPages = currentPossibilityTotalPages > 6 ? 6 : currentPossibilityTotalPages;
						
						LoggerManager.logInfo("REDUCED Total pages: " + currentPossibilityTotalPages.toString());
					} 
					else if(!isNaN(numberPlasmidCode) && searchYears.contains(numberPlasmidCode.toString())) 
					{
						LoggerManager.logInfo("Plasmid code is a known year: " + numberPlasmidCode.toString());
						
						currentPossibilityTotalPages = currentPossibilityTotalPages > 6 ? 6 : currentPossibilityTotalPages;
						
						LoggerManager.logInfo("REDUCED Total pages: " + currentPossibilityTotalPages.toString());
					}
				}
			}
			else
			{
				LoggerManager.logError("Watch out... Something wrong here!! no total records! " + responseText);
				
				if(responseText.search('id="gs_captcha_f"') > -1)
				{
					LoggerManager.logError("Dude.. this is a captcha!");
					
					this.dispatcher.dispatchEvent(new SearchManagerEvent(SearchManagerEvent.CAPTCHA_DETECTED));
					return false;
				}
			}
			
			return true;
		}
		
		private var currentlyParsingResults:Array;
		private function parseResults(responseText:String):void
		{
			// All results
			var results:ArrayCollection = new ArrayCollection(responseText.split(ParseVariables.RESULT_BLOCK));
			
			// Remove first result since it will be nothing (pre-result)
			results.removeItemAt(0);
			
			// This is the total results for the current page!!
			LoggerManager.logInfo(results.length.toString());
			currentlyParsingResults = [];
			
			// Now lets parse keys
			for each(var result:String in results)
			{
				var googleResult:ScholarResult = new ScholarResult();
				// Lets find the url key
				// class="gs_rt"
				
				var link:String = "";
				var title:String = "";
				if(result.split(ParseVariables.LINK_BLOCK).length > 1)
					link = result.split(ParseVariables.LINK_BLOCK)[1];
				
				// Split the beginning of the link
				if(link.split(ParseVariables.LINK_CLEANUP).length > 1)
				{
					link = link.split(ParseVariables.LINK_CLEANUP)[1];
					title = link;
				}
				
				// Split the closing of the link
				link = link.split('"')[0];
				googleResult.link = link;
				
				// We got the link of the record! lets parse the journals
				var journalInfo:String = "";
				if(result.split(ParseVariables.JOURNAL_BLOCK).length > 1)
					journalInfo = result.split(ParseVariables.JOURNAL_BLOCK)[1];
				
				// Split the closing
				journalInfo = journalInfo.split(ParseVariables.DIV_CLEANUP)[0];
				
				// Now we parse the citations section
				var citationsInfo:String = "";
				if(result.split(ParseVariables.CITATION_BLOCK).length > 1)
					citationsInfo = result.split(ParseVariables.CITATION_BLOCK)[1];
				citationsInfo = citationsInfo.split(ParseVariables.DIV_CLEANUP)[0];
				googleResult.citationInfo = citationsInfo;
				
				// Set citations number
				var citations:int = 0;
				if(citationsInfo.split("Cited by ").length > 1)
					citations = int(citationsInfo.split("Cited by ")[1].split("<")[0]);
				googleResult.citations = citations;
				
				// Set the year
				for(var i:uint=0; i<searchYears.length; i++)
				{
					if(journalInfo.search(searchYears[i]) > -1)
					{
						googleResult.year = searchYears[i];
						
						LoggerManager.logInfo(googleResult.year);
						break;
					}
				}
				
				googleResult.journalInfo = journalInfo;
				
				// Set the journal
				var journal:String = "";
				journal = parseJournal(googleResult);
				googleResult.journal = journal;
				
				// Set the authors
				var authors:String = "";
				authors = parseAuthors(googleResult);
				googleResult.authors = authors;
				
				// Set the title of the publication
				if(title.split("</a>").length > 1)
					title = title.split("</a>")[0];
				if(title.split(">").length > 1)
					title = title.split(">")[1];
				if (title.substr(-1) == '.') // check whether title includes a dot in the end, and delete it if so
					title = title.slice(0,-1);
				googleResult.title = title;
				
				// Store the unique result
				uniqueResults[link] = googleResult;
				
				// Store citation key and id
				
				if(result.split("return gs_ocit(event,'").length > 1)
				{
					googleResult.citationKey = String(result.split("return gs_ocit(event,'")[1]).split("','")[0];
					googleResult.citationNumber = String(String(result.split("return gs_ocit(event,'")[1]).split("','")[1]).split("')")[0];
				
					// Found a case that the number is not available, this should be called
					if(!isNaN(Number(googleResult.citationNumber)))
					{
						currentlyParsingResults.push(googleResult);
					}
				}
			}
			
			// Lets load and parse citations
			loadCitations();
		}
		
		private function loadCitations(event:Event=null):void
		{
			if(searchAborted)
				return;
			
			if(currentlyParsingResults.length == 0)
			{
				setTimeout(continueCodeSearch, requestDelay);
				return;
			}
			
			// Start loading citations
			for each(var loadedResult:ScholarResult in currentlyParsingResults)
			{
				if(!loadedResult.detailsLoaded)
				{
					loadedResult.addEventListener(ScholarResultEvent.COMPLETE, loadCitations);
					loadedResult.addEventListener(ScholarResultEvent.SKIP, loadCitations);
					
					// Handle captcha properly
					loadedResult.addEventListener(ScholarResultEvent.CAPTCHA_DETECTED, function (evt:ScholarResultEvent):void
					{
						this.dispatcher.dispatchEvent(new SearchManagerEvent(SearchManagerEvent.CAPTCHA_DETECTED));
					});
					
					// Citation is not available, lets try second result
					loadedResult.addEventListener(ScholarResultEvent.ERROR, function(errorEvt:ScholarResultEvent):void
					{
						currentPossibilityHasErrors = true;
						loadCitations();
					});
					
					if(event && event is ScholarResultEvent && 
						(event as ScholarResultEvent).type == ScholarResultEvent.SKIP)
					{
						loadedResult.loadCitations(sqlManager);
					}
					else
					{
						setTimeout(loadedResult.loadCitations, randomRange(requestDelay/2, requestDelay*3/2), sqlManager);
					}
					
					return;
				}
			}
			
			// Seems all citations got loaded, lets continue searching
			setTimeout(continueCodeSearch, randomRange(requestDelay/2, requestDelay*3/2));
		}
		
		private function parseHighImpactJournals(result:ScholarResult):void
		{
			//Isolate Journal name only
			var journalName:String = "";
			if(result.journalInfo.indexOf(' - ') >= 0)
			{
				if(result.journalInfo.split(' - ').length > 1)
					journalName = result.journalInfo.split(' - ')[1].split(/, (\d+)/)[0];
				
			} else {
				journalName = result.journalInfo;
			}
			
			for each(var journalKeyword:String in journalKeywords)
			{
				//TODO: improve search with regexp to match for either identical journalKeyword and journalName or
				// partial journalKeyword and journalName including &hellip. Otherwise include specific journal in search
				if(isHighImpact(result.journal.toLowerCase(), journalKeyword.toLowerCase()))
				{
					//Keyword found
					result.highImpactJournalsCount ++;
					result.highImpactJournals.push(journalKeyword);
				}
			}
		}
		
		private function continueCodeSearch():void
		{
			if(searchAborted)
				return;
			
			// Search Next!
			if(currentPossibilityCurrentPage < currentPossibilityTotalPages)
			{
				setTimeout(performSearch, randomRange(requestDelay/2, requestDelay*3/2), (currentPossibilityCurrentPage * 20), plasmidCode);
				currentPossibilityCurrentPage++;
			} 
			else
			{
				// Search completed! stop!
				// Lets count high impact journals
				for each(var result:ScholarResult in uniqueResults)
				{
					parseHighImpactJournals(result);
					
					LoggerManager.logInfo("HIJ " + result.highImpactJournalsCount);
				}
				
				setTimeout(resultHandler, 100);
			}
		}
		
		// parse the result info for the authors
		private function parseAuthors(result:ScholarResult):String
		{
			var info:String = result.journalInfo;
			var authors:String = info;
			
			if(authors.split(' - ').length > 1) 
			{
				authors = authors.split(' - ')[0];
			}
			// RegExp for matching all HTML tags
			var removeHtmlRegExp:RegExp = new RegExp("<[^<]+?>", "gi");
			authors = authors.replace(removeHtmlRegExp, "");
			while (authors.indexOf('&hellip;') >= 0) 
			{
				authors = authors.replace('&hellip;', '...');
			}
			while (authors.indexOf('&amp;') >= 0) 
			{
				authors = authors.replace('&amp;', '&');
			}
			while (authors.indexOf('&#39;') >= 0) 
			{
				authors = authors.replace('&#39;', '`');
			}
				
			return authors;
		}
		
		// parse the result info for the journal
		private function parseJournal(result:ScholarResult):String
		{
			var journal:String = "";
			if(result.journalInfo.indexOf(' - ') >= 0)
			{
				if(result.journalInfo.split(' - ').length > 1)
					journal = result.journalInfo.split(' - ')[1].split(/, (\d+)/)[0];
				while (journal.indexOf('&hellip;') >= 0) 
					journal = journal.replace('&hellip;', '...');
				while (journal.indexOf('&amp;') >= 0) 
				{
					journal = journal.replace('&amp;', '&');
				}
				while (journal.indexOf('&#39;') >= 0) 
				{
					journal = journal.replace('&#39;', '`');
				}
			} else {
				journal = "NA"; // we have observed that journal comes always after the authors
			}
			// Check whether journal is simply a number, i.e the field contained only a year, not journal,year
			var testFlag:Boolean = isNaN(Number(journal));
			if(!testFlag) journal = "NA";
			
			return journal;
		}
		
		//Checks whether a Journal keyword matches the parsed Journal name information 
		private function isHighImpact(info: String, key:String):Boolean
		{
			if(info == key)
				return true;
			/*else if (info.indexOf('  &hellip;') >= 0)
			{
				info = info.split('  &hellip;')[0];
				var wordCount:int = info.match(/[^\s]+/g).length;
				var keyArray:Array = key.split(/\s+/);
				if (keyArray.length > wordCount) keyArray.length = wordCount;
				var keyTruncated:String = keyArray.join(" ");
				if(info == keyTruncated) return true; //equality behaves better than .search()
			}
			else if (info.indexOf('&hellip;  ') >= 0)
			{
				info = info.split('&hellip;  ')[1];
				var wordCountB:int = info.match(/[^\s]+/g).length;
				var keyArrayB:Array = key.split(/\s+/);
				// for the case when "..." comes first, so we need to truncate from the end of the array
				if (keyArrayB.length > wordCountB) keyArrayB.splice(0, keyArrayB.length-wordCountB);
				var keyTruncatedB:String = keyArrayB.join(" ");
				if(info == keyTruncatedB) return true; //equality behaves better than .search()
			}*/
			return false;
		}
		
		////////////////////////////// Utility methods ////////////////////////
		// TODO: move to parent class
		/**
		 * Generic error handler
		 */
		private function onGenericError(event:Event=null):void
		{
			// Notify of error
			setTimeout(faultHandler, 100, event);
		}
		
		protected function randomRange(minNum:Number, maxNum:Number):Number 
		{
			var randomNumber:Number = (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
			return (randomNumber && !isNaN(randomNumber)) ? randomNumber : maxNum;
		}
		
		protected function getParams(keyword:String, startFrom:Number):Object
		{
			var params:Object = new Object();
			params[SearchParameters.CITATIONS] = "1";
			params[SearchParameters.PATENTS] = ConfigurationsManager.sharedManager.confs.patentIncluded;
			params[SearchParameters.FROM_YEAR] = ConfigurationsManager.sharedManager.confs.fromYear;
			params[SearchParameters.TO_YEAR] = ConfigurationsManager.sharedManager.confs.toYear;
			params[SearchParameters.PAGE_SIZE] = "20";
			params[SearchParameters.KEYWORD] = keyword;
			params[SearchParameters.START_FROM] = startFrom.toString();
			
			LoggerManager.logInfo(params[SearchParameters.FROM_YEAR] + " - " + params[SearchParameters.TO_YEAR])
			return params;
		}
	}
}