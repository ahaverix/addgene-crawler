package managers.parsers.events
{
	import flash.events.Event;
	
	public class PageParserEvent extends Event
	{
		public static var COMPLETE:String = "page_parser_complete";
		public static var ERROR:String = "page_parser_error";
		
		public function PageParserEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}