package managers.parsers.events
{
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	public class SearchManagerEvent extends Event
	{
		public static var COMPLETED:String = "search_manager_complete";
		public static var PROGRESS:String = "search_manager_progress";
		public static var CAPTCHA_DETECTED:String = "captcha_detected";
		
		private var _plasmidCode:String;
		private var _plasmidSearchResults:Dictionary;
		private var _searchHasErrors:Boolean = false;
		
		public function SearchManagerEvent(type:String, plasmidCode:String="", searchResults:Dictionary=null, hasErrors:Boolean = false, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this._plasmidCode = plasmidCode;
			this._plasmidSearchResults = searchResults;
			this._searchHasErrors = hasErrors;
		}
		
		public function get PlasmidCode():String
		{
			return _plasmidCode;
		}
		
		public function get SearchResults():Dictionary 
		{
			return _plasmidSearchResults;
		}
		
		public function get SearchHasErrors():Boolean 
		{
			return _searchHasErrors;
		}
	}
}