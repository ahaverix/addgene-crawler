package models
{
	public class Plasmid
	{
		// class variables
		public var id:int = -1;
		public var code:String;
		public var status:String = "";
		public var total:String = "0";
		
		// initializers
		public function Plasmid(dbRecord:Object=null)
		{
			if(dbRecord)
			{
				id = dbRecord.plasmid_id;
				code = dbRecord.plasmid_code;
				status = dbRecord.status;
				total = dbRecord.total_results;
			}
			else
			{
				code = "";
			}
		}
	}
}