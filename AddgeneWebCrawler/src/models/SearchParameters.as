package models
{
	public class SearchParameters
	{
		/*
		Parameters: q = Keyword (encoded)
		as_ylo=2014 ---> From year
		as_yhi=2015 ----> To year
		as_vis=0 ----> Include citations
		as_sdt=0 ---> Include patents
		num=5 --> Number of results in a page
		start=60 --> Starting record
		*/
		public static var KEYWORD:String = "q";
		public static var FROM_YEAR:String = "as_ylo";
		public static var TO_YEAR:String = "as_yhi";
		public static var CITATIONS:String = "as_vis";
		public static var PATENTS:String = "as_sdt";
		public static var PAGE_SIZE:String = "num";
		public static var START_FROM:String = "start";
		
		public static var DEFAULT_PAGE_SIZE:Number = 20;
	}
}