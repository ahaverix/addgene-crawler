package models
{
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.utils.StringUtil;
	
	import events.ScholarResultEvent;
	
	import managers.log.LoggerManager;
	import managers.services.ICrawlerService;
	import managers.services.impl.GoogleScholarServices;
	import managers.sql.Query;
	import managers.sql.SQLManager;

	public class ScholarResult extends Sprite
	{
		// TODO: Generate this automatically instead of hardcoded
		private const searchYears:Array = ["2004","2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015","2016"];

		public var year:String = "";
		public var link:String = "";
		public var journalInfo:String = "";
		public var highImpactJournalsCount:Number = 0;
		public var highImpactJournals:Array = new Array();
		public var citationInfo:String = "";
		public var citations:int = 0;
		public var authors:String = "";
		public var journal:String = "";
		public var title:String = "";
		
		public var citationKey:String = "";
		public var citationNumber:String = "";
		
		public var service:ICrawlerService;
		public var detailsLoaded:Boolean = false;
		public var isCited:Boolean = false;
		
		public function loadCitations(sqlManager:SQLManager):void
		{
			if(citationKey && citationKey.length > 0 && 
				citationNumber && citationNumber.length > 0)
			{
				sqlManager.executeQuery(Query.SELECT_DETAILEDVIEW_BY_KEY.replace("$key", link), function(result:SQLEvent):void
				{
					var records:SQLResult = (result.currentTarget as SQLStatement).getResult();
					
					if(records && records.data && records.data.length >= 1)
					{
						var storedRecord:Object = records.data[0];
						
						if(String(storedRecord.isFullCitation) == "1")
						{
							year = storedRecord.year;
							journal = storedRecord.journal;
							authors = storedRecord.authors;
							isCited = true;
							
							LoggerManager.logInfo("Found result in db, no need to request it again! " + journal + " || " + authors)
							
							detailsLoaded = true;
							dispatchEvent(new ScholarResultEvent(ScholarResultEvent.SKIP));
							return;
						}
					}
					
					requestScholarCitation();
				}, function(evt:SQLErrorEvent):void
				{
					requestScholarCitation();
				});
			}
		}
		
		private function requestScholarCitation():void
		{
			service = new GoogleScholarServices();
			service.getResultCitation(citationKey, citationNumber, onGetCitations, function(evt:FaultEvent):void {
				LoggerManager.logError("Error getting citations");
				
				detailsLoaded = true;
				// Notify to handle captcha or continue searching
				dispatchEvent(new ScholarResultEvent(ScholarResultEvent.ERROR, evt));
			});
		}
		
		private function onGetCitations(event:ResultEvent):void
		{
			citationInfo = String(event.result);
			detailsLoaded = true;
			
			if(isCaptcha(citationInfo))
				return;
				
			if(citationInfo && citationInfo.length > 0)
			{
				if(citationInfo.split("Vancouver</th><td>").length > 1)
				{
					citationInfo = String(citationInfo.split("Vancouver</th><td>")[1]);
					
					if(citationInfo.split('class="gs_citr">').length > 1)
					{
						citationInfo = (citationInfo.split('class="gs_citr">')[1]).split("</td></tr>")[0];	
					}
				}
				
				while(citationInfo.indexOf("<i>") > -1)
					citationInfo = citationInfo.replace("<i>","");
				
				while(citationInfo.indexOf("</i>") > -1)
					citationInfo = citationInfo.replace("</i>","");
				
				while(citationInfo.indexOf("<div>") > -1)
					citationInfo = citationInfo.replace("<div>","");
				
				while(citationInfo.indexOf("</div>") > -1)
					citationInfo = citationInfo.replace("</div>","");
				
				// Clean up RTL span
				citationInfo = citationInfo.replace("<span dir=rtl>","").replace("</span>", "");
			}
			
			LoggerManager.logInfo("CITATIONSSS: " + citationInfo);
			
			// Add parsing logic here
			// Authors
			if(citationInfo.indexOf('. ') >= 0)
			{
				if(citationInfo.split('. ').length > 1)
					authors = citationInfo.split('. ')[0];
				while (authors.indexOf('&hellip;') >= 0) 
					authors = authors.replace('&hellip;', '...');
				while (authors.indexOf('&amp;') >= 0) 
					authors = authors.replace('&amp;', '&');
				while (authors.indexOf('&#39;') >= 0) 
					authors = authors.replace('&#39;', '`');
			} else {
				authors = citationInfo.length > 1 ? citationInfo : "NA"; 
				LoggerManager.logInfo("Only authors in the citation info");
			}
			
			// Journal
			// Clear citationInfo of parenthesis text using \(([^\)]*[^\(]*)\)+
			//trace("\\(([^)]*[^(]*)\\)");
			var removeParenthesesRegExp:RegExp = new RegExp("\\(([^)]*[^(]*)\\)", "g");
			var cleanInfo:String = citationInfo;
			cleanInfo = cleanInfo.replace(removeParenthesesRegExp, "");
			if(citationInfo.indexOf('. ') >= 0)
			{
				if(citationInfo.split(title).length > 1) //we split by the title
				{
					LoggerManager.logInfo("Full title from the results, splitting by title to parse journal.");
					journal = citationInfo.split(title)[1];
					if(journal.indexOf('. ') >= 0)
					{
						journal = journal.split('. ')[1];
						while (journal.indexOf('&hellip;') >= 0) 
							journal = journal.replace('&hellip;', '...');
						while (journal.indexOf('&amp;') >= 0) 
							journal = journal.replace('&amp;', '&');
						while (journal.indexOf('&#39;') >= 0) 
							journal = journal.replace('&#39;', '`');
						if(journal.indexOf('; ') >= 0)
							journal = journal.split('; ')[0]; // hanlding case: "ProQuest; 2007."
						var pubYear:String = " " + year; 
						if(journal.indexOf(pubYear) >= 0)
							journal = journal.split(pubYear)[0]; // handling case when year is added to the journal with a single space (occuring often)
						if (journal.substr(-1) == ',') // check whether journal includes a comma in the end, and delete it if so
							journal = journal.slice(0,-1);
						if (journal.substr(-1) == '.') // check whether journal includes a dot in the end, and delete it if so
							journal = journal.slice(0,-1);
					} else {
						journal = "NA";
					}
				} 
				else if(cleanInfo.split('. ').length > 2) //if title is not full (bcs of...) we use cleaninfo to avoid special cases like: Tangapo A. Transformasi dan Ekspresi Transien Gen Pelapor Gusa pada Andrographis paniculata (Burm. F.) Wallich Ex Ness (Transformation and Expression of Reporter Gene Gusa on Andrographis paniculata (Burm. F.) Wallich Ex Ness). BIOSLOGOS. 2012 Jan 2;2(1).
				{
					var count:int = 0;
					var titleSplit:Array = title.split('. '); //find the number of occurrences of ". " displayed in the title to minimize the error 
					count = titleSplit.length - 1;
					LoggerManager.logInfo("Not full title, splitting by dots count to parse journal. Partial title counts " + count + " dots.");
					if (cleanInfo.split('. ').length > (count+2))
					{
						journal = cleanInfo.split('. ')[count+2]; // skip the dots that belong to the title
						// I observed a few cases where there are even more dots hidden after the ... in the title, and that messes up the parsing. 
						// I thought of this extra way to handle this cases. We check for the substrings split at dot after count+2 dots, for one containing "journal"
						var infoSplit:Array = cleanInfo.split('. ');
						for (var j:uint=count+2; j<infoSplit.length; j++)
						{
							if (infoSplit[j].indexOf('Journal') >= 0)
								journal = infoSplit[j];
						}
						while (journal.indexOf('&hellip;') >= 0) 
							journal = journal.replace('&hellip;', '...');
						while (journal.indexOf('&amp;') >= 0) 
							journal = journal.replace('&amp;', '&');
						while (journal.indexOf('&#39;') >= 0) 
							journal = journal.replace('&#39;', '`');
						if(journal.indexOf('; ') >= 0)
							journal = journal.split('; ')[0]; // hanlding case: "ProQuest; 2007."
						var pubYears:String = " " + year; 
						if(journal.indexOf(pubYears) >= 0)
							journal = journal.split(pubYears)[0]; // handling case when year is added to the journal with a single space (occuring often)
						if (journal.substr(-1) == ',') // check whether journal includes a comma in the end, and delete it if so
							journal = journal.slice(0,-1);
						if (journal.substr(-1) == '.') // check whether journal includes a dot in the end, and delete it if so
							journal = journal.slice(0,-1);
					} else {
						journal = "NA";
					}
				} else {
					journal = "NA";
				}
			} else {
				journal = "NA"; 
			}
			
			// In case we don't have a parsed year, lets try to find it..
			if(StringUtil.trim(String(year)).length == 0)
			{
				LoggerManager.logInfo("Replacing empty year from citations");
				
				// Set the year
				for(var i:uint=0; i<searchYears.length; i++)
				{
					if(citationInfo.search(" " + searchYears[i] + " ") > -1)
					{
						year = searchYears[i];
						
						LoggerManager.logInfo("Found: " + year);
						break;
					}
				}
			}
			
			isCited = true;
			// Notify
			this.dispatchEvent(new ScholarResultEvent(ScholarResultEvent.COMPLETE));
		}
		
		private function isCaptcha(responseText:String):Boolean
		{
			if(responseText.search('id="gs_captcha_f"') > -1)
			{
				LoggerManager.logError("Dude.. this is a captcha!");
				
				this.dispatchEvent(new ScholarResultEvent(ScholarResultEvent.CAPTCHA_DETECTED));
				return true;
			}
			
			return false;
		}
		
		public function ScholarResult()
		{
		}
	}
}