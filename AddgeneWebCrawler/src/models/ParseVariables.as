package models
{
	public class ParseVariables
	{
		public static var TOTAL_RESULTS:String = 'id="gs_ab_md">';
		public static var RESULT_BLOCK:String = 'class="gs_r"';
		public static var LINK_BLOCK:String = 'class="gs_rt"';
		public static var JOURNAL_BLOCK:String = 'class="gs_a">';
		public static var CITATION_BLOCK:String = 'class="gs_fl">';
		
		public static var TOTAL_RESULTS_CLEANUP:String = 'result';
		public static var LINK_CLEANUP:String = '<a href="';
		public static var DIV_CLEANUP:String = '</div>';
	}
}