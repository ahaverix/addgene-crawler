package models
{
	public class Record
	{
		// class variables
		public var id:int = -1;
		public var code:String;
		public var allPublicationsCount:Number;
		public var highImpactJournalsCount:Number;
		public var year:String;
		
		// initializers
		public function Record(dbRecord:Object=null)
		{
			if(dbRecord)
			{
				id = dbRecord.crawl_id;
				code = dbRecord.plasmid_code;
				allPublicationsCount = Number(dbRecord.all_publications);
				highImpactJournalsCount = Number(dbRecord.high_impact_journals);
				year = dbRecord.year;
			}
			else
			{
				code = "";
				allPublicationsCount = 0;
				highImpactJournalsCount = 0;
				year = "";
			}
		}
		
		// TODO: implement save and some utility methods
	}
}