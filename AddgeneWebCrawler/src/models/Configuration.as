package models
{
	import mx.utils.StringUtil;
	
	import managers.log.LoggerManager;

	public class Configuration
	{
		public var fromYear:String = "2004";
		public var toYear:String = "2016";
		public var patentIncluded:String = "1";
		public var delay:String = "10000";
		public var processingLimit:Number = 100;
		
		public function get Delay():Number
		{
			var tmpValue:Number = Number(delay);
			
			return isNaN(tmpValue) ? 10000 : tmpValue;
		}
		
		public var keywords:Array = ['"plasmid $code"','"plasmid* $code"','"plasmid** $code"','"plasmid*** $code"',
			'"addgene $code"','"addgene* $code"','"addgene** $code"','"addgene*** $code"'];
		[Bindable]
		public var journals:Array = ["Biochemical", "Biochemical Journal", "Biological Chemistry", "Journal of Biological Chemistry",
			"ACS", "ACS Synthetic Biology", "ACS Chemical Biology", "Scientific Reports", "Nucleic Acids Research", "PNAS", "EMBO", "EMBO Journal", "The EMBO Journal", "Journal of the American Chemical Society", "American Chemical Society", 
			"Cell", "Science", "Nature Reviews Genetics", "Nature Reviews Molecular Cell Biology", "Molecular Cell Biology", "Nature", "Nature Biotechnology"]; 
		
		public function Configuration(configurationCotent:String=null)
		{
			if(!configurationCotent)
				return;
			
			// Parse the content
			var keys:Array = configurationCotent.split("\n");
			
			for each(var key:String in keys)
			{
				switch(key.split("=")[0].toLowerCase())
				{
					case "fromyear":
						fromYear = key.split("=")[1];
						break;
					case "toyear":
						toYear = key.split("=")[1];
						break;
					case "delay":
						delay = key.split("=")[1];
						break;
					case "includepatent":
						patentIncluded = key.split("=")[1];
						break;
					case "keywords":
						keywords = String(key.split("=")[1]).split("|");
						break;
					case "joournals":
						journals =  String(key.split("=")[1]).split("|");
						break;
					case "plasmidslimit":
						processingLimit =  isNaN(Number(String(key.split("=")[1]).split("|"))) ? 100 : Number(String(key.split("=")[1]).split("|"));
						break;
				}
			}
			
			// Remove empty values
			var tmpKeywords:Array = new Array();
			for each(var value:String in keywords)
			{
				if(StringUtil.trim(value).length > 0)
				{
					tmpKeywords.push(StringUtil.trim(value));
				}
			}
			keywords = tmpKeywords;
			
			var tmpJournals:Array = new Array();
			for each(var value1:String in journals)
			{
				if(StringUtil.trim(value1).length > 0)
				{
					tmpJournals.push(StringUtil.trim(value1));
				}
			}
			journals = tmpJournals;
		}
		
		
		/**
		 * Prepare file content
		 */
		public function get content():String
		{
			keywords = keywords ? keywords : [];
			journals = journals ? journals : [];
			
			var storedContent:String = "";
			
			storedContent += "fromyear" + "=" + fromYear + "\n";
			storedContent += "toyear" + "=" + toYear + "\n";
			storedContent += "delay" + "=" + delay + "\n";
			storedContent += "includepatent" + "=" + patentIncluded + "\n";
			storedContent += "keywords" + "=" + keywords.join("|") + "\n";
			storedContent += "joournals" + "=" + journals.join("|") + "\n";
			storedContent += "plasmidslimit" + "=" + processingLimit;
			
			LoggerManager.logInfo(storedContent);
			return storedContent;
		}
	}
}